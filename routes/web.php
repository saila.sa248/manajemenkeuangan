<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard','AdminController@index')->name('dashboard');

Route::get('/login','AuthController@index')->name('form_login');
Route::post('/signin', 'AuthController@sendLoginRequest')->name('ceklogin');
Route::get('/logout','AuthController@logout')->name('logout_action');

// PENDAPATAN
Route::get('/pendapatan','PendapatanController@index')->name('tampil_pendapatan');
Route::get('pendapatan/create','PendapatanController@createData')->name('create_pendapatan');
Route::post('pendapatan/postcreate','PendapatanController@postData')->name('post_pendapatan');
Route::get('pendapatan/edit/{id}','PendapatanController@editData')->name('edit_pendapatan');
Route::post('pendapatan/update/{id}','PendapatanController@updateData')->name('up_pendapatan');
Route::get('pendapatan/delete/{id}', 'PendapatanController@deleteData')->name('delete_data');
Route::get('pendapatan/cari','PendapatanController@caripendapatan')->name('cari_pendapatan');

// PENGELUARAN
Route::get('/pengeluaran','PengeluaranController@index')->name('tampil_pengeluaran');
Route::get('pengeluaran/create','PengeluaranController@createData')->name('create_pengeluaran');
Route::post('pengeluaran/post','PengeluaranController@postData')->name('post_pengeluaran');
Route::get('pengeluaran/edit/{id}','PengeluaranController@editData')->name('edit_pengeluaran');
Route::post('pengeluaran/update/{id}','PengeluaranController@updateData')->name('up_pengeluaran');
Route::get('pengeluaran/delete/{id}', 'PengeluaranController@deleteData')->name('delete_data');

// KEBUTUHAN
Route::get('/kebutuhan','KebutuhanController@index')->name('tampil_kebutuhan');
Route::get('kebutuhan/create','KebutuhanController@createData')->name('create_kebutuhan');
Route::post('kebutuhan/post','KebutuhanController@postData')->name('post_kebutuhan');
Route::get('kebutuhan/edit/{id}','KebutuhanController@editData')->name('edit_kebutuhan');
Route::post('kebutuhan/update/{id}','KebutuhanController@updateData')->name('up_kebutuhan');
Route::get('kebutuhan/delete/{id}', 'KebutuhanController@deleteData')->name('delete_data');