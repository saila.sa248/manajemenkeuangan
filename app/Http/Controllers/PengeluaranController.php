<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PengeluaranModel;
use App\KategoriModel;
use App\KebutuhanModel;

class PengeluaranController extends Controller
{
    //
    public function index(){

        $data = PengeluaranModel::all()->where('is_active','1');

        return view('admin.pengeluaran.index', compact('data'));
    }

    public function createData()
    {
        $cat = KategoriModel::all();

        $keb = KebutuhanModel::all();

        return view('admin.pengeluaran.tambah', compact('cat','keb'));
    }

    public function postData(Request $request, PengeluaranModel $pengeluaranModel)
    {
        $simpan = $pengeluaranModel->create([
            'id_pengeluaran' => $request->id_pengeluaran,
            'id_category' => $request->id_category,
            'id_kebutuhan' => $request->id_kebutuhan,
            'tgl_pengeluaran' => $request->tgl_pengeluaran,
            'jumlah' => $request->jumlah,
            'keterangan' => $request->keterangan,
            'is_active' => 1,
        ]);

        if (!$simpan->exists) {
            return redirect()->route('tampil_pengeluaran')->with('error', 'Data Gagal Disimpan');
        }
        return redirect()->route('tampil_pengeluaran')->with('success', 'Data Berhasil Disimpan');
    }

    public function editData($id)
    {
        $data = PengeluaranModel::where('id_pengeluaran', $id)->first();
        $cat = KategoriModel::all();
        $keb = KebutuhanModel::all();

        return view('admin.pengeluaran.edit', compact('data','cat','keb'));
    }

    public function updateData($id, Request $request, PengeluaranModel $pengeluaranModel)
    {
        $simpan = $pengeluaranModel->where('id_pengeluaran', $id)->update([
            'id_category' => $request->id_category,
            'id_kebutuhan' => $request->id_kebutuhan,
            'tgl_pengeluaran' => $request->tgl_pengeluaran,
            'jumlah' => $request->jumlah,
            'keterangan' => $request->keterangan,
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_pengeluaran')->with('error', 'Data Gagal di Update');
        }

        return redirect()->route('tampil_pengeluaran')->with('success', 'Data Berhasil di Update');
    }

    public function deleteData($id, PengeluaranModel $pengeluaranModel)
    {
        $delete = $pengeluaranModel->where('id_pengeluaran', $id)->update([
            'is_active' => '0',
        ]);

        if (!$delete) {
            return redirect()->route('tampil_pengeluaran')->with('error', 'Data Gagal di Hapus');
        }

        return redirect()->route('tampil_pengeluaran')->with('success', 'Data Berhasil di Hapus');
    }
}
