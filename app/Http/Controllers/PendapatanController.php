<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PendapatanModel;
use App\KategoriModel;
use DB;

class PendapatanController extends Controller
{
    //
    public function index(){

        $data = PendapatanModel::all()->where('is_active','1');

        return view('admin.pendapatan.index', compact('data'));
    }

    public function createData()
    {
        $cat = KategoriModel::all();

        return view('admin.pendapatan.tambah', compact('cat'));
    }

    public function postData(Request $request, PendapatanModel $pendapatanModel)
    {
        $simpan = $pendapatanModel->create([
            'id_pendapatan' => $request->id_pendapatan,
            'id_category' => $request->id_category,
            'tgl_pendapatan' => $request->tgl_pendapatan,
            'jumlah' => $request->jumlah,
            'keterangan' => $request->keterangan,
            'is_active' => 1,
        ]);

        if (!$simpan->exists) {
            return redirect()->route('tampil_pendapatan')->with('error', 'Data Gagal Disimpan');
        }
        return redirect()->route('tampil_pendapatan')->with('success', 'Data Berhasil Disimpan');
    }

    public function editData($id)
    {
        $data = PendapatanModel::where('id_pendapatan', $id)->first();
        $cat = KategoriModel::all();

        return view('admin.pendapatan.edit', compact('data','cat'));
    }

    public function updateData($id, Request $request, PendapatanModel $pendapatanModel)
    {
        $simpan = $pendapatanModel->where('id_pendapatan', $id)->update([
            'id_category' => $request->id_category,
            'tgl_pendapatan' => $request->tgl_pendapatan,
            'jumlah' => $request->jumlah,
            'keterangan' => $request->keterangan,
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_pendapatan')->with('error', 'Data Gagal di Update');
        }

        return redirect()->route('tampil_pendapatan')->with('success', 'Data Berhasil di Update');
    }

    public function deleteData($id, PendapatanModel $pendapatanModel)
    {
        $delete = $pendapatanModel->where('id_pendapatan', $id)->update([
            'is_active' => '0',
        ]);

        if (!$delete) {
            return redirect()->route('tampil_pendapatan')->with('error', 'Data Gagal di Hapus');
        }

        return redirect()->route('tampil_pendapatan')->with('success', 'Data Berhasil di Hapus');
    }

    public function caripendapatan(Request $request)
    {
        $caripendapatan = $request->caripendapatan;

        // $data = PendapatanModel::all()->wehere('id_pendapatan', 'like', '%' .$caripendapatan. '%');

        $data = DB::table('pendapatan')->where('id_pendapatan', 'like', '%' .$caripendapatan. '%')->paginate();

        // $data = DB::table('pendapatan')
        //     ->join('pembelian', 'suplier.kd_suplier', '=', 'pembelian.kd_suplier')
        //     ->select('suplier.kd_suplier', 'suplier.nm_suplier', 'pembelian.id_pembelian', 'pembelian.tgl_beli', 'suplier.perusahaan', 'pembelian.grand_total')
        //     ->distinct()
        //     ->where('suplier.perusahaan', 'like', '%' . $caripembelian . '%')
        //     ->orwhere('pembelian.id_pembelian', 'like', '%' . $caripembelian . '%')
        //     ->paginate();

        return view('admin.pendapatan.index', compact('data'));
    }
}
