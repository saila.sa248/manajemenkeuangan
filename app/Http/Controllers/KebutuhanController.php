<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KategoriModel;
use App\KebutuhanModel;

class KebutuhanController extends Controller
{
    //
    public function index(){

        $data = KebutuhanModel::all()->where('is_active','1');

        return view('admin.kebutuhan.index', compact('data'));
    }

    public function createData()
    {
        $cat = KategoriModel::all();

        return view('admin.kebutuhan.tambah', compact('cat'));
    }

    public function postData(Request $request, KebutuhanModel $kebutuhanModel)
    {
        $simpan = $kebutuhanModel->create([
            'id_kebutuhan' => $request->id_kebutuhan,
            'id_category' => $request->id_category,
            'keterangan' => $request->keterangan,
            'is_active' => 1,
        ]);

        if (!$simpan->exists) {
            return redirect()->route('tampil_kebutuhan')->with('error', 'Data Gagal Disimpan');
        }
        return redirect()->route('tampil_kebutuhan')->with('success', 'Data Berhasil Disimpan');
    }

    public function editData($id)
    {
        $data = KebutuhanModel::where('id_kebutuhan', $id)->first();
        $cat = KategoriModel::all();

        return view('admin.kebutuhan.edit', compact('data','cat'));
    }

    public function updateData($id, Request $request, KebutuhanModel $kebutuhanModel)
    {
        $simpan = $kebutuhanModel->where('id_kebutuhan', $id)->update([
            'id_category' => $request->id_category,
            'keterangan' => $request->keterangan,
        ]);

        if (!$simpan) {
            return redirect()->route('tampil_kebutuhan')->with('error', 'Data Gagal di Update');
        }

        return redirect()->route('tampil_kebutuhan')->with('success', 'Data Berhasil di Update');
    }

    public function deleteData($id, KebutuhanModel $kebutuhanModel)
    {
        $delete = $kebutuhanModel->where('id_kebutuhan', $id)->update([
            'is_active' => '0',
        ]);

        if (!$delete) {
            return redirect()->route('tampil_kebutuhan')->with('error', 'Data Gagal di Hapus');
        }

        return redirect()->route('tampil_kebutuhan')->with('success', 'Data Berhasil di Hapus');
    }
}

