<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengeluaranModel extends Model
{
    //
    protected $table = 'pengeluaran';

    protected $fillable = ['id_pengeluaran','id_category', 'id_kebutuhan', 'tgl_pengeluaran', 'jumlah','keterangan','is_active'];

    public function haveKategori()
    {
        return $this->belongsTo(KategoriModel::class, 'id_category', 'id_category');
    }

    public function haveKebutuhan()
    {
        return $this->belongsTo(KebutuhanModel::class, 'id_kebutuhan', 'id_kebutuhan');
    }
}
