<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PendapatanModel extends Model
{
    //
    protected $table = 'pendapatan';

    protected $fillable = ['id_pendapatan','id_category', 'keterangan', 'tgl_pendapatan', 'jumlah','is_active'];

    public function haveKategori()
    {
        return $this->belongsTo(KategoriModel::class, 'id_category', 'id_category');
    }
}
