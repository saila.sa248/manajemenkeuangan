<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KebutuhanModel extends Model
{
    //
    protected $table = 'kebutuhan';

    protected $fillable = ['id_kebutuhan','id_category', 'keterangan','is_active'];

    public function hasManyPengeluaran()
    {
        return $this->hasMany(PengeluaranModel::class, 'id_kebutuhan', 'id_kebutuhan'); //(Model::class, 'foreignkey', 'primarykey')
    }

    public function haveKategori()
    {
        return $this->belongsTo(KategoriModel::class, 'id_category', 'id_category');
    }
}

