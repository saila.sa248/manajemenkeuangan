<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriModel extends Model
{
    //
    protected $table = 'kategori';

    public function hasManyPendapatan()
    {
        return $this->hasMany(PendapatanModel::class, 'id_category', 'id_category'); //(Model::class, 'foreignkey', 'primarykey')
    }
    
    public function hasManyPengeluaran()
    {
        return $this->hasMany(PengeluaranModel::class, 'id_category', 'id_category'); //(Model::class, 'foreignkey', 'primarykey')
    }
    
    public function hasManyKebutuhan()
    {
        return $this->hasMany(KebutuhanModel::class, 'id_kebutuhan', 'id_kebutuhan'); //(Model::class, 'foreignkey', 'primarykey')
    }
}
