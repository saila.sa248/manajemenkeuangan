@extends('admin.layouts.master')
@section('content')
<div class="x_panel mt-5">
    <div class="x_title">
        <h2>Edit Data Pendapatan</h2>
        <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i
                        class="fa fa-wrench"></i></a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="#">Settings 1</a>
                    <a class="dropdown-item" href="#">Settings 2</a>
                </div>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
        </ul>
        <div class="clearfix">
            <div class="x_content">

                <form class="form-label-left input_mask" method="post" action="{{route('up_pendapatan',$data->id_pendapatan)}}">
                    {{csrf_field()}}

                    <div class="col-md-6 col-sm-12 mt-5  form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="inputSuccess2"
                            value="{{$data->id_pendapatan}}" name="id_pendapatan" require="id_pendapatan" readonly>
                        <span class="fa fa-circle form-control-feedback left" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-12 mt-5  form-group has-feedback">
                        <select class=" form-control" id="id_category" name="id_category" require="id_category">
                            <option>Kategori</option>
                            @foreach($cat as $c)
                            <option value="{{ $c->id_category }}">{{ $c->id_category }} -- {{$c->nama}}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-6 col-sm-12 mt-3  form-group has-feedback">
                        <input type="datetime" class="form-control has-feedback-left" id="inputSuccess4"
                            placeholder="Tanggal" name="tgl_pendapatan" value="{{$data->tgl_pendapatan}}" require="tgl_pendapatan">
                        <span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-12 mt-3  form-group has-feedback">
                        <input type="teks" class="form-control" id="inputSuccess5" placeholder="Jumlah" name="jumlah" require="jumlah"
                            value="{{$data->jumlah}}">
                        <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                    </div>

                    <div class="col-md-6 col-sm-12 mt-3  form-group has-feedback">
                        <input type="teks" class="form-control" id="inputSuccess5" placeholder="Keterangan"
                            name="keterangan" value="{{$data->keterangan}}" require="keterangan">
                        <span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
                    </div>

                    <!-- <div class="col-md-6 col-sm-12 mt-3  form-group has-feedback">
                        <select class=" form-control" name="is_active" value="Active">
                            <option>Active - {{$data->is_active}}</option>
                            <option value="0">0</option>
                            <option value="1">1</option>
                        </select>
                    </div> -->

                    <div class="col-md-6 col-sm-12 mt-3  form-group has-feedback">
                        <input type="date" class="form-control" id="inputSuccess5" placeholder="{{$data->updated_at}}"
                            name="updated_at" require="updated_at" value="{{$data->updated_at}}">
                    </div>

                    <div class="ln_solid"></div>
                    <div class="form-group row">
                        <div class="col-md-9 col-sm-9  offset-md-3 mt-3">
                            <button class="btn btn-primary float-right" type="reset">Reset</button>
                            <button type="submit" class="btn btn-success float-right">Submit</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
@endsection