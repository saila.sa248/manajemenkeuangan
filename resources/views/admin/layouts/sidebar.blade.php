<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
    <div class="menu_section">
        <h3>Menu</h3>
        <ul class="nav side-menu">
            <li><a href="{{ route('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>

            <li><a><i class="fa fa-table"></i> Keuangan <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="{{route('tampil_pendapatan')}}">Pendapatan</a></li>
                    <li><a href="{{route('tampil_pengeluaran')}}">Pengeluaran</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-edit"></i> Rencana <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#">Kategori</a></li>
                    <li><a href="{{route('tampil_kebutuhan')}}">Kebutuhan</a></li>
                </ul>
            </li>
            <li><a><i class="fa fa-desktop"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                <ul class="nav child_menu">
                    <li><a href="#">Pendapatan</a></li>
                    <li><a href="#">Pengeluaran</a></li>
                    <li><a href="#">Keseluruhan</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>