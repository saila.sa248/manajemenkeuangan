@extends('admin.layouts.master')
@section('content')
<div class="page-title">
    <div class="title_left">
        <h3>DATA <small>KEBUTUHAN</small></h3>
    </div>


    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <form method="get" action="#">
                <div class="input-group">
                    <input type="text" name="cari" value="{{ old('cari') }}" class="form-control"
                        placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button">Go!</button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="x_panel">
    <div class="x_title">
        <a href="{{route('create_kebutuhan')}}">
            <h2><i class="fa fa-plus-square"></i> Tambah Kebutuhan</h2>
        </a>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="col-12">
            @if(session()->has('error'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session()->get('error')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
        </div>
        <div class="col-12">
            @if(session()->has('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session()->get('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
        </div>
        <div class="table-responsive">
            <table class="table table-striped jambo_table bulk_action text-center">
                <thead>
                    <tr class="headings">
                        <th class="column-title"> ID Kebutuhan </th>
                        <th class="column-title"> Kategori </th>
                        <th class="column-title"> Keterangan </th>
                        <th class="column-title no-link last"><span class="nobr">Aksi</span>
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($data as $d)
                    <tr class="even pointer">
                        <td class=" ">{{ $d->id_kebutuhan }}</td>
                        <td class=" ">{{ $d->haveKategori->nama }} </td>
                        <td class=" ">{{ $d->keterangan }}</td>
                        <td class=" last">
                            <div class="btn-group">
                                <a href="{{route('edit_kebutuhan',$d->id_kebutuhan)}}" class="btn btn-secondary btn-sm text-white" type="button">Ubah</a>
                                <a href="{{route('delete_data',$d->id_kebutuhan)}}" class="btn btn-secondary btn-sm text-white" type="button">Hapus</a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection