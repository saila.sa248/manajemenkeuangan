<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengeluaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengeluaran', function (Blueprint $table) {
            $table->string('id_pengeluaran')->primary();
            $table->string('id_category');
            $table->foreign('id_category')->references('id_category')->on('kategori');
            $table->string('id_kebutuhan');
            $table->foreign('id_kebutuhan')->references('id_kebutuhan')->on('kebutuhan');
            $table->date('tgl_pengeluaran');
            $table->string('jumlah', 100);
            $table->string('keterangan', 100);
            $table->tinyInteger('is_active')->nullable()->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengeluaran');
    }
}
