<?php

use Illuminate\Database\Seeder;

class PendapatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pendapatan')->insert(array(
            array(
                'id_pendapatan' => 'PD-001',
                'id_category' => 'CT-001',
                'keterangan' => 'Full tanpa potongan',
                'jumlah' => '3000000',
                'tgl_pendapatan' => now(),
                'is_active' => 1,
                'created_at' => now()
            ),
        ));
    }
}
