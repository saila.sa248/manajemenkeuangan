<?php

use Illuminate\Database\Seeder;

class KebutuhanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('kebutuhan')->insert(array(
            array(
                'id_kebutuhan' => 'BT-001',
                'id_category' => 'CT-001',
                'keterangan' => 'Travelling',
                'is_active' => 1,
                'created_at' => now()
            ),
        ));
    }
}
