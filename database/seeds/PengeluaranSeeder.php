<?php

use Illuminate\Database\Seeder;

class PengeluaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('pengeluaran')->insert(array(
            array(
                'id_pengeluaran' => 'PL-001',
                'id_category' => 'CT-001',
                'id_kebutuhan' => 'BT-001',
                'tgl_pengeluaran' => now(),
                'jumlah' => '1000000',
                'keterangan' => 'Penting',
                'is_active' => 1,
                'created_at' => now()
            ),
        ));
    }
}
