<?php

use Illuminate\Database\Seeder;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('kategori')->insert(array(
            array(
                'id_category' => 'CT-001',
                'nama' => 'Gaji Pokok',
                'is_active' => 1,
                'created_at' => now(),
            ),
            array(
                'id_category' => 'CT-002',
                'nama' => 'Passive Income',
                'is_active' => 1,
                'created_at' => now(),
            ),
            array(
                'id_category' => 'CT-003',
                'nama' => 'Bonus',
                'is_active' => 1,
                'created_at' => now(),
            ),
        ));
    }
}
